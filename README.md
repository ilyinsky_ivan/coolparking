## Welcome!
Cool Parking ASP.NET Web API client-server application presented here

This is automatic system for parking. Client can leave car at parking and system will withdraw money.
Different vehicle types and tariffs for them are provided. Parking owner can see all transactions and save them to file. 

To run it .Net Core 3.1 is required.

Result:

![Result:](https://i.ibb.co/tXQMYCT/result.gif)

Have a nice day :)