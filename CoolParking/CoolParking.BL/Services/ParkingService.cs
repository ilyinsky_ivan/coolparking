﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly Parking _parking = Parking.Instance;

        private readonly List<TransactionInfo> _transactions = new List<TransactionInfo>();

        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly ILogService _logService;

        //constructor for DI
        public ParkingService()
        {
            _withdrawTimer = new TimerService();
            _logTimer = new TimerService();
            _logService = new LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log");
            SetUp();
        }

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;
            SetUp();
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return _parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return _parking.Capacity - _parking.Vehicles.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(_parking.Vehicles);
        }

        public Vehicle GetVehicle(string vehicleId)
        {
            return _parking.Vehicles.SingleOrDefault(vehicle => vehicle.Id == vehicleId);
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if(vehicle == null)
                throw new ArgumentException("Error: vehicle is null!");
            if (GetFreePlaces() == 0)
                throw new InvalidOperationException("Error: there are no free places at the parking now!");
            if(_parking.Vehicles.Find(x => x.Id == vehicle.Id) != null)
                throw new ArgumentException($"Error: vehicle with ID: {vehicle.Id} is already at parking!");
            _parking.Vehicles.Add(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = _parking.Vehicles.FirstOrDefault(vecl => vecl.Id == vehicleId);
            if (vehicle == null)
                throw new ArgumentException($"Error: vehicle with ID: {vehicleId} wasn't found!");
            if (vehicle.Balance < 0)
                throw new InvalidOperationException($"Error: you should top up your balance to take it out!");
            _parking.Vehicles.Remove(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0)
                throw new ArgumentException("Sum can't be negative!");
            var vehicle = _parking.Vehicles.FirstOrDefault(vecl => vecl.Id == vehicleId);
            if (vehicle == null)
                throw new ArgumentException($"Vehicle with ID = {vehicleId} was not found and was not topped up!");
            vehicle.Balance += sum;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactions.ToArray();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void Dispose()
        {
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
            _transactions.Clear();
            _parking.Vehicles.Clear();
            _parking.Balance = Settings.ParkingBalance;
        }

        private static decimal GetRate(Vehicle vehicle)
        {
            var rate = Settings.PriceRates[vehicle.VehicleType];
            if (vehicle.Balance < rate)
                rate = GetPenaltyRate(rate, vehicle.Balance);
            return rate;
        }

        private static decimal GetPenaltyRate(decimal usualRate, decimal balance)
        {
            return Settings.FineCoefficient * (usualRate - balance) + balance;
        }

        public List<TransactionInfo> WithdrawPayment()
        {
            var transactions = new List<TransactionInfo>(_parking.Vehicles.Count);
            foreach (var vehicle in _parking.Vehicles)
            {
                var rate = GetRate(vehicle);

                transactions.Add(new TransactionInfo(vehicle.Id, rate));
                vehicle.Balance -= rate;
                _parking.Balance += rate;
            }
            return transactions;
        }

        private void WithdrawPaymentFromVehicles(object sender, ElapsedEventArgs args)
        {
            _transactions.AddRange(WithdrawPayment());
        }

        private void WriteTransactionsToLog(object sender, ElapsedEventArgs args)
        {
            var sb = new StringBuilder(_transactions.Count);
            _transactions.ForEach(transaction => sb.Append(transaction + "\n"));

            _logService.Write(sb.ToString());
            _transactions.Clear();
        }

        private void SetUp()
        {
            _withdrawTimer.Interval = Settings.TransactionPeriod;
            _withdrawTimer.Elapsed += WithdrawPaymentFromVehicles;
            _withdrawTimer.Start();
            _logTimer.Interval = Settings.LogWritingPeriod;
            _logTimer.Elapsed += WriteTransactionsToLog;
            _logTimer.Start();
        }
    }
}