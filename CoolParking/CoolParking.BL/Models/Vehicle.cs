﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; private set; }
        public VehicleType VehicleType { get; private set; }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (balance < 0)
                throw new InvalidOperationException("Balance can't be negative!");
            if (!ValidateId(id))
                throw new ArgumentException("ID doesn't correspond to pattern!");
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public static bool ValidateId(string id)
        {
            var regex = new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}");
            return regex.IsMatch(id);
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            return GenerateTwoRandomLetters() + '-' + GenerateRandomNumberValue() + '-' + GenerateTwoRandomLetters();
        }

        private static string GenerateTwoRandomLetters()
        {
            var rand = new Random();
            var letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            var word = "";
            for (var i = 0; i < 2; i++)
            {
                var letterNum = rand.Next(0, letters.Length - 1);
                word += letters[letterNum];
            }

            return word;
        }

        private static string GenerateRandomNumberValue()
        {
            const int min = 1000;
            const int max = 9999;
            var rand = new Random();

            return rand.Next(min, max).ToString();
        }

        public override string ToString()
        {
            return $"Vehicle ID: {Id}\nVehicle type: {VehicleType}\nVehicle balance: {Balance}";
        }
    }
}
