﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        public static Parking Instance => lazy.Value;

        public decimal Balance { get; internal set; } = Settings.ParkingBalance;
        public int Capacity { get; internal set; } = Settings.ParkingCapacity;
        public List<Vehicle> Vehicles { get; } = new List<Vehicle>(Settings.ParkingCapacity);

        private Parking() { }

        private static readonly Lazy<Parking> lazy =
            new Lazy<Parking>(() => new Parking());

    }
}