﻿using System;
using System.Collections.ObjectModel;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<ReadOnlyCollection<Vehicle>> GetVehicles()
        {
            return Ok(_parkingService.GetVehicles());
        }

        [HttpGet("{id}")]
        [Produces("application/json")]
        public ActionResult<string> GetVehicle(string id)
        {
            if (!Vehicle.ValidateId(id))
                return BadRequest("Error: incorrect vehicle ID!");
            var vehicle = _parkingService.GetVehicle(id);
            if (vehicle == null)
                return NotFound($"Error: vehicle with ID: {id} wasn't found!");
            return Ok(vehicle);
        }

        [HttpPost]
        public ActionResult<Vehicle> AddVehicle(JObject value)
        {
            if (value == null)
                return BadRequest("Error: some vehicle information was missed");
            try
            { 
                var vehicle = JsonConvert.DeserializeObject<Vehicle>(value.ToString());
                _parkingService.AddVehicle(vehicle);
                return StatusCode(201, vehicle);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (InvalidOperationException)
            {
                return StatusCode(500);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult RemoveVehicle(string id)
        {
            if (!Vehicle.ValidateId(id))
                return BadRequest("Error: incorrect vehicle ID!");
            try
            {
                _parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (InvalidOperationException)
            {
                return StatusCode(500);
            }
        }
    }
}