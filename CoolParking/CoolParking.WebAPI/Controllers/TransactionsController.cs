﻿using System;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("last")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<string> GetLast()
        {
            var transactions = _parkingService.GetLastParkingTransactions();
            return Ok(transactions);
        }

        [HttpGet("all")]
        [Produces("application/json")]
        public ActionResult<string> GetAll()
        {
            try
            {
                var info = _parkingService.ReadFromLog();
                return Ok(info);
            }
            catch (InvalidOperationException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> TopUp(JObject infoJObject)
        {
            var info = new {id = "", sum = 0.0m};
            info = JsonConvert.DeserializeAnonymousType(infoJObject.ToString(), info);
            if (info?.id == null || !Vehicle.ValidateId(info.id) || info.sum <= 0)
                return BadRequest("Error: incorrect vehicle ID or sum!");
            try
            {
                _parkingService.TopUpVehicle(info.id, info.sum);
                var vehicle = _parkingService.GetVehicle(info.id);
                return Ok(vehicle);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}