﻿using System;
using System.Threading.Tasks;

namespace CoolParking.CUI
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("---------------------------Cool Parking Main Menu---------------------------\n\n"
                                  + "Get parking balance -> 1\n"
                                  + "Get current incomes -> 2\n"
                                  + "Get amount of free/occupied spaces -> 3\n"
                                  + "Get all current transactions -> 4\n"
                                  + "Get history of transactions -> 5\n"
                                  + "Get list of vehicles at parking -> 6\n"
                                  + "Put vehicle -> 7\n"
                                  + "Take vehicle -> 8\n"
                                  + "Top up vehicle balance -> 9\n"
                                  + "Exit -> 'e'\n");
                var choice = InputHandler.GetCharFromUser(input => input == 'e' || input >= '1' || input <= '9');
                Console.Clear();
                if (choice == 'e')
                {
                    Console.WriteLine("Bye bye");
                    break;
                }

                var result = await Result(choice);
                Console.Clear();
                Console.WriteLine(result + "\n\nPress any key to continue");
                Console.ReadKey();
            }

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        private static async Task<string> Result(char input) => input switch
        {
            '1' => await InputHandler.GetParkingBalance(),
            '2' => await InputHandler.GetCurrentIncomes(),
            '3' => await InputHandler.GetPlacesInfo(),
            '4' => await InputHandler.GetCurrentTransactions(),
            '5' => await InputHandler.GetTransactionsHistory(),
            '6' => await InputHandler.GetVehicles(),
            '7' => await InputHandler.AddVehicle(),
            '8' => await InputHandler.RemoveVehicle(),
            '9' => await InputHandler.TopUpBalance(),
            _ => "Incorrect input!"
        };

    }
}
