﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using Newtonsoft.Json;

namespace CoolParking.CUI
{
    internal class InputHandler
    {
        private static readonly HttpClient _client = new HttpClient();
        private static bool _flag = false;
        static InputHandler()
        {
            _client.BaseAddress = new Uri("http://localhost:8080/api/");
            _client.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        internal static async Task<string> GetParkingBalance()
        {
            return "Parking balance is: " + await _client.GetStringAsync("parking/balance");
        }

        internal static async Task<string> GetPlacesInfo()
        {
            var free = await _client.GetStringAsync("parking/freePlaces");
            var capacity = await _client.GetStringAsync("parking/capacity");

            return $"Free: {free}\nOccupied:{int.Parse(capacity) - int.Parse(free)}";
        }

        internal static async Task<string> GetVehicles()
        {
            var response = await _client.GetStringAsync("vehicles");
            try
            {
                var vehicles = JsonConvert.DeserializeObject<List<Vehicle>>(response);
                if (vehicles == null)
                    return "No more vehicles";
                const string separator = "\n-------------------------\n";
                var result = string.Join(separator, vehicles.Select(vehicle => vehicle.ToString()));
                return "All vehicles:\n\n" + result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return ex.Message;
            }
        }

        internal static async Task<string> AddVehicle()
        {
            var response = await _client.GetStringAsync("parking/freePlaces");
            if (!int.TryParse(response, out var free) || free == 0)
                return "No free places now, come back later";
            _flag = false;
            var id = CreateVehicleId();
            if (_flag)
                return "Aborting";
            var type = GetVehicleType();
            if (_flag)
                return "Aborting";
            var balance = GetSumForBalance();
            if (_flag)
                return "Aborting";
            try
            {
                var vehicle = new Vehicle(id, type, balance);
                var request = JsonConvert.SerializeObject(vehicle);
                await _client.PostAsync("vehicles", new StringContent(request, Encoding.UTF8, "application/json"));
                return vehicle.ToString();
            }
            catch (Exception ex)
            {
                return "Failed:\n" + ex.Message;
            }
        }

        internal static async Task<string> RemoveVehicle()
        {
            _flag = false;
            var id = GetVehicleIdFromUser();
            if (_flag)
                return "Aborting";
            try
            {
                await _client.DeleteAsync("vehicles/" + id);
                return $"Vehicle with ID: {id} was successfully picked up";
            }
            catch (Exception ex)
            {
                return "Failed:\n" + ex.Message;
            }
        }

        internal static async Task<string> GetCurrentIncomes()
        {
            var response = await _client.GetStringAsync("transactions/last");
            var transactions = JsonConvert.DeserializeObject<ReadOnlyCollection<TransactionInfo>>(response);
            var incomes = transactions.Select(tr => tr.Sum).Sum();
            return "Parking current incomes: " + incomes;
        }
        
        internal static async Task<string> GetCurrentTransactions()
        {
            var response = await _client.GetStringAsync("transactions/last");
            var transactions = JsonConvert.DeserializeObject<ReadOnlyCollection<TransactionInfo>>(response);
            if (transactions.Count == 0)
                return "No transactions yet";
            var transactionsList = string.Join("\n", transactions.Select(tr => tr.ToString()));
            return "Transactions:\n" + transactionsList;
        }

        internal static async Task<string> GetTransactionsHistory()
        {
            var result = await _client.GetStringAsync("transactions/all");
            if (string.IsNullOrWhiteSpace(result))
                return "Transactions log is empty";
            return "Transactions:\n" + result;
        }

        internal static async Task<string> TopUpBalance()
        {
            _flag = false;
            var id = GetVehicleIdFromUser();
            if (_flag)
                return "Aborting";
            var sum = GetSumForBalance();
            if (_flag)
                return "Aborting";
            try
            {
                var requestBody = new StringContent(JsonConvert.SerializeObject(new { id, sum }));
                await _client.PutAsync("transactions/topUpVehicle", requestBody);
                return $"Vehicle with ID: {id} was topped up on {sum}";
            }
            catch (ArgumentException ex)
            {
                return "Failed:\n" + ex.Message;
            }
        }

        private static string CreateVehicleId()
        {
            const string random = "random";
            const string exit = "exit";
            Console.Clear();
            Console.WriteLine("Enter id of your vehicle (required format LL-NNNN-LL where L - uppercase letter, N - uppercase number)\n"
                + $"Enter {random} to generate id automatically\n"
                + $"Enter {exit} to go exit\n");
            var result = Console.ReadLine();
            if (result == exit)
                _flag = true;
            return result != random ? result : Vehicle.GenerateRandomRegistrationPlateNumber();
        }

        private static VehicleType GetVehicleType()
        {
            Console.Clear();
            Console.WriteLine("Put type of vehicle:\n"
                + "1. PassengerCar\n"
                + "2. Truck\n"
                + "3. Bus\n"
                + "4. Motorcycle\n"
                + "e. Exit\n");
            var choice = GetCharFromUser(ic => ic == 'e' || ic >= '1' && ic <= '4');
            if (choice != 'e')
                return choice switch
                {
                    '1' => VehicleType.PassengerCar,
                    '2' => VehicleType.Truck,
                    '3' => VehicleType.Bus,
                    '4' => VehicleType.Motorcycle,
                    _ => throw new ArgumentException("You should never see this")
                };
            _flag = true;
            return default;
        }

        private static decimal GetSumForBalance()
        {
            Console.Clear();
            Console.WriteLine("Enter sum for adding:\n");
            var result = GetDecimalNumberFromUser(input => input == -1 || input > 0);
            if (result == -1)
                _flag = true;
            return result;
        }

        private static string GetVehicleIdFromUser()
        {
            const string exit = "exit";
            Console.Clear();
            Console.WriteLine($"Enter id of your vehicle to park (required format LL-NNNN-LL where L - uppercase letter, N - uppercase number) or \"{exit}\" to exit: ");
            var id = Console.ReadLine();
            if (id == exit)
                _flag = true;
            return id;
        }

        internal static char GetCharFromUser(Predicate<char> condition)
        {
            while (true)
            {
                Console.Write("Enter your choice -> ");
                var input = Console.ReadKey().KeyChar;
                if (condition(input))
                    return input;
            }
        }

        internal static decimal GetDecimalNumberFromUser(Predicate<decimal> condition)
        {
            while (true)
            {
                var input = Console.ReadLine();
                if (decimal.TryParse(input, out var result) && condition(result))
                    return result;
            }
        }
    }
}
